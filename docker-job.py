from flask import Flask, jsonify, request, make_response
from crontab import CronTab
import boto
import json
import code
import time

app = Flask(__name__)

dockers = []

tasks = [{'id': 1},{'id': 2}]

# Given a Docker image, the desired date-time and a list of ENV Variables, 
# schedule the provisioning of a docker container for that image at the time
@app.route('/schedule', methods=['POST'])
def create_task():
    task={
        'date': request.json['date'],
        'hour': request.json['hour'],
        'minute': request.json['minute'],
        'ENV': request.json['ENV'],
        'done': False
    }
    cron = CronTab(user='root')
    job = cron.new(command='curl -XGET http://localhost/createdocker')
    job.minute.on(request.json['minute'])
    job.hour.on(request.json['hour'])
    job.day.on(request.json['date'])
    cron.write()
    tasks.append(task)
    return jsonify({'task': task}), 201

@app.route('/createdocker', methods=['GET'])
#Create EC2 with docker
 
def create_ec2():
    #Conecct to EC2
    user_data = """#!/bin/bash
    yum update -y 
    yum install -y docker 
    service docker start
    usermod -a -G docker ec2-user 
    docker pull lftoledo/project-nubank 
    docker run -d -p 80:80 lftoledo/project-nubank"""
    ec2 = boto.connect_ec2()
    interface = boto.ec2.networkinterface.NetworkInterfaceSpecification(subnet_id='subnet-16b05c4e',
                                                                         groups=['sg-53af5528'],
                                                                         associate_public_ip_address=True)
    interfaces = boto.ec2.networkinterface.NetworkInterfaceCollection(interface)
    reservation = ec2.run_instances(instance_type='t2.micro',
                                    image_id='ami-f5f41398',
                                    key_name='docker',
                                    network_interfaces=interfaces,
                                    #subnet_id='subnet-16b05c4e',
                                    user_data=user_data)

    instance = reservation.instances[0]
    instance.update()

    while instance.state == "pending":
        time.sleep(5)
        instance.update()

    return jsonify(instance=instance.public_dns_name), 201

# list the scheduled jobs
@app.route('/list', methods=['GET'])
def get_list():
    cron = CronTab(user='root')
    scheduled = cron.find_command('curl -XGET http://localhost/createdocker')
    #print scheduled[0]
    #code.interact(local=dict(globals(),**locals()))
    linhas=[]

    for line in cron.lines:
        if str(line):
            linhas.append(str(line))

    data = { "valor": linhas }
    jsondata = json.dumps(data)
    resp = make_response(jsondata, 201)
    resp.mimetype= 'application/json'

    return jsondata

# show the status for one job mostrar o status dos ec2
@app.route('/status', methods=['GET'])
def get_tasks2():
    return jsonify({'tasks': tasks})

# To be called by the scheduled application when it finishes processing = status da instancia para SNS
@app.route('/callback', methods=['GET'])
def get_tasks3():
    return jsonify({'tasks': tasks})

# We only need this for local development.
if __name__ == '__main__':
    app.run(
        host="0.0.0.0",
        port=int("80"),
        debug=True)

