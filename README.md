# README #

### This is a restfull api for schedule a job container ###

Everybody wants to use a management tool for yours containers, so don't go after kubernetes or 
tsuru, use docker-jobs!


### How to use ###

It is very easy to use, all you need is a machine linux, python 2.7.9 and a AWS account.

## Configuration
  * git clone https://luiz_toledo@bitbucket.org/luizteam/docker-job.git
  * cd docker-job
  * virtualenv env
  * source env/bin/active
  * pip install flask crontab jsonify boto

## Dependencies
flask

## Test
To schedule a job, you need to do a POST like this:

        curl -i -H "Content-Type: application/json" -X POST -d '{"date":"27","hour":"14","minute":"10","ENV": "PROD"}' http://localhost/schedule

To list the scheduled JOBS:

        curl -X GET http://localhost/list